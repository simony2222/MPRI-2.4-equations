(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

module Make (S : sig
  type t
end) =
struct
  module Signature = struct
    type 'a t =
      | Get of (S.t -> 'a)
      | Set of S.t * (unit -> 'a)

    (* NYI *)
    let map f s = match s with
    | Get h -> Get (fun s -> f (h s))
    | Set (x,h) -> Set (x, fun () -> f( h ()))
  end

  module FreeState = Free.Make (Signature)
  include FreeState

  (* Define [Signature] so that ['a FreeState.t] is isomorphic to the
          following type:

     <<<
          type 'a t =
            | Return of 'a
            | Get of unit * (S.t -> 'a t)
            | Set of S.t * (unit -> 'a t)
     >>>
  *)

  let get () = op (Get (fun s -> s))

  let set s = op (Set (s, fun () -> ()))

  let run m = failwith "NYI"
end
