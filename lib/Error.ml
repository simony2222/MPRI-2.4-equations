(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Signature = struct
  type 'a t = |

  (* NYI *)
  let map f s = failwith "NYI"
end

module Error = Free.Make (Signature)
include Error

  (* Define [Signature] so that ['a FreeError.t] is isomorphic to the
          following type:

     <<<
          type 'a t =
            | Return of 'a
            | Error of exn * (void -> 'a t)
     >>>
  *)

let err e = failwith "NYI"

let run m = failwith "NYI"
